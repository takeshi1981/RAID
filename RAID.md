# :dolphin: LAID

## 1. RAIDレベル

- **Linear Mode**  
・複数のディスクが後ろに追加される形で１台のディスクとして扱えるようにするもの。１つのディスクがいっぱいになると次のディスクへ書きこまれる。各ディスクの容量が異なる場合でも無駄なくディスクを使用できる。  
・１台のディスクがクラッシュするとすべてのデータを失う場合がある。  

      mdadm --create --verbose /dev/md0 --level=linear --raid-devices=2 /dev/vdb /dev/vdc

- **RAID0(Stripe Mode)**  
・ストライピングによって複数ディスクを１台のディスクのように扱えるようにするもの。複数台のディスクに読み書き処理を*同時並行的*におこなうことでアクセス速度を高速化する。台数は多いほど大容量化、高速化する。  
・データの復元性は備えておらず、複数のディスクのうち、どれか 1 台がクラッシュすると、ディスク全体が読み出し不能になる。

      mdadm --create --verbose /dev/md1 --level=0 --raid-devices=2 /dev/vdd /dev/vde

- **RAID1(Mirroring Mode)**  
・ミラーリングを使って*同一のデータを複数のディスクに書き込み*、一方のディスクが故障しても、他方で処理を続行できるようにするもの。通常はディスク2台を使う。  
・同一のデータを2台のディスクに書き込むため、ディスクの使用効率は 50% になってしまう。

      mdadm --create --verbose /dev/md2 --level=1 --raid-devices=2 /dev/vdf /dev/vdg

- **RAID5**  
・3 台以上のディスクに*データとパリティ情報を記録する*ことで耐障害性を高めたもの。データをディスクに記録する際、そのデータのパリティ情報を生成し、残りのデータとともにストライピングによって複数のディスクに分散して書き込む。

      mdadm --create --verbose /dev/md1 --level=5 --raid-devices=3 /dev/vdd /dev/vde /dev/vdf

- **RAID10**
・RAID1「ミラーリング」の構成をさらにRAID0「ストライピング」で掛け合わせた構成。RAID1を高速化し耐障害性を高めた構成となるが、RAID1同様に利用可能容量は総ハードディスク容量の半分となる。

      mdadm --create --verbose /dev/md0 --level=10 --raid-devices=3 /dev/vdb /dev/vdc /dev/vdd

## 2. RAID操作コマンド

- **RAIDの状態確認**

      cat /proc/mdstat
      mdadm --detail --scan
      mdadm --detail /dev/mdX

- **RAIDの停止**

      mdadm --stop /dev/mdX

- **ディスクにRAIDパーティションが残っていた場合のディスクの初期化**  

      mdadm --zero-superblock /dev/vdX

- **RAIDからデバイスを切り離し（非アクティブに）**

      mdadm –fail /dev/md1 /dev/sdaX

- **RAIDからデバイスを取り除く（–fail後のみ実行可能）**

      mdadm --remove /dev/md0 /dev/vdX

- **RAIDデバイスの追加**

      mdadm --add /dev/md0 /dev/vdX

- **アクティブなデバイス数の変更（RAID0, RAID1のみ）**  

      mdadm –grow /dev/mdX –raid-devices=2
